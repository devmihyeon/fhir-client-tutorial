# FHIR Client tutorial

### 1. HAPI FHIR JPA 서버 설치
 - ROOT.war 다운로드
 - 커맨드 창을 켠 후 다운로드한 파일이 있는 디렉터리로 이동
 - java -jar ROOT.war 입력하여 실행

### 2. FHIR Client 실습용 SpringBoot 프로젝트 실행
 - fhir-client-tutorial.zip 다운로드
 - 다운로드한 파일 압축 해제
 - 압축 해제한 파일을 STS에서 import한 후 실행